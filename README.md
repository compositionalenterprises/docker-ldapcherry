# LdapCherry

[LdapCherry documentation on ReadTheDoc](http://ldapcherry.readthedocs.org/en/latest/)
[LdapCherry source code on GitHub](https://github.com/kakwa/ldapcherry)

## Docker

## Building and running

```bash
# Build the docker container with the tag ldapcherry
$ docker build -t ldapcherry .

# Run the docker container tagged as ldapcherry with the demo backend
# and allow incoming requests on port 8080 on the localhost
$ docker run -p 8080:8080 ldapcherry
```

## Default environment variables

| Environment Variable Name | Description | Default | Values |
| --- | --- | --- | --- |
| `DEBUG` | Run the container in debug mode     | `False`             | * `True`              |
| | | | * `False`             |
| `SUFFIX` | Set the suffix for the domain | `dc=example,dc=org` | * `example.org` |
|                             |                                     |                       | * `dc=example,dc=org` |
| `SERVER_SOCKET_HOST`      | IP address for the daemon to run on | `0.0.0.0`           | IP Address              |
| `SERVER_SOCKET_PORT`      | Port for the daemon to run on       | `8080`              | Unprivileged Port       |
| `LOG_ACCESS_HANDLER`      | The target for the access logs      | `stdout`            | * `stdout`            |
|                             |                                     |                       | * `file`              |
|                             |                                     |                       | * `syslog`            |
|                             |                                     |                       | * `none`              |
| `LOG_ERROR_HANDLER`       | The target for the error logs       | `stdout`            | * `stdout`            |
|                             |                                     |                       | * `file`              |
|                             |                                     |                       | * `syslog`            |
|                             |                                     |                       | * `none`              |

#### NOTE:

**Setting either of the `LOG_<TYPE>_HANDLER` variables to `file` requires the appropriate `LOG_<TYPE>_FILE` to be set**

### Other environment variables

All other configuration options are parsed programmatically from environment variables that are formatted differently for the two file types -- one way for the `ini` file and another for the `.yml` file.

# INI configuration file

The environment variables that should be passed to the `ldapcherry.ini` configuration file are only to be made into upper-case underscore-separated versions of the options inside of each section of the `ldapcherry.ini` file. For instance:

- `server.socket_host` -> `SERVER_SOCKET_HOST`
- `request.show_tracebacks` -> `REQUEST_SHOW_TRACEBACKS`
- `tools.sessions.timeout` -> `TOOLS_SESSIONS_TIMEOUT`
- `min_length` -> `MIN_LENGTH`

They will be put into their respective sections in the `ldapcherry.ini` file.

## YAML configuration files

For the `yaml` configuration files (`attributes.yml` and `roles.yml`), the environment variable name is programmatically parsed based on the following template:

###  `<FILENAME (without the .yml extension)>__<ATTRIBUTE ID>__<PARAMETER>`

The following example demonstrates how to customize the `shell` attribute ID in the `attributes.yml` file:

```yaml
shell:
    description: "Shell of the user"
    display_name: "Shell"
    weight: 80
    values:
        - /bin/bash
        - /bin/zsh
        - /bin/sh
```

```bash
ATTRIBUTES__SHELL__DESCRIPTION="Shell of the user"
ATTRIBUTES__SHELL__DISPLAY_NAME="Shell"
ATTRIBUTES__SHELL__WEIGHT="80"
ATTRIBUTES__SHELL__VALUES="['/bin/bash', '/bin/zsh', '/bin/sh']"
