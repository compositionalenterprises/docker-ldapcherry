#!/bin/sh

wget -O /tmp/tags https://api.github.com/repos/kakwa/ldapcherry/tags 2>/dev/null;

cat /tmp/tags | grep 'name' | head -n 1 | cut -d '"' -f 4 > /tmp/release_tag;

cat /tmp/release_tag;
