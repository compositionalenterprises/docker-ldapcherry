FROM debian:stretch

ARG release_tag=1.1.1

RUN apt-get -y update && apt-get install -y git
RUN git clone --single-branch --branch ${release_tag} https://github.com/kakwa/ldapcherry /opt/ldapcherry
WORKDIR "/opt/ldapcherry"
RUN apt-get install -y python-dev python-pip libldap2-dev libsasl2-dev libssl-dev && \
    pip install -e /opt/ldapcherry -r /opt/ldapcherry/requirements-stretch.txt pycodestyle passlib coveralls configparser in_place && \
    /usr/bin/python2 /opt/ldapcherry/setup.py install
ADD . /opt/ldapcherry/

VOLUME /etc/ldapcherry
EXPOSE 8080

CMD ["/usr/bin/python2", "/opt/ldapcherry/init.py"]
